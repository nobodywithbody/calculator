package calculator

class Number : TokenInterface, Compounded {
    var value = ""

    override fun canBeFirst(): Boolean = true

    override fun canCompound(token: TokenInterface): Boolean = when (token) {
        is Number -> true
        else -> false
    }

    override fun makeToken(char: Char): TokenInterface {
        val num = Number()
        num.value = char.toString()

        return num
    }

    override fun compound(token: TokenInterface) {
        if (token is Number) value += token.value
    }

    override fun canBeAfter(token: TokenInterface): Boolean = when (token) {
        is Number, is Variable -> false
        else -> true
    }

    override fun canRepresent(char: Char): Boolean = char.isDigit()
}