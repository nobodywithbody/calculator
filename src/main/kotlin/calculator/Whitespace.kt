package calculator

class Whitespace : TokenInterface {
    override fun canCompound(token: TokenInterface): Boolean = false
    override fun canRepresent(char: Char): Boolean = char.isWhitespace()
    override fun canBeFirst(): Boolean = true
    override fun makeToken(char: Char): TokenInterface {
        return Whitespace()
    }

    override fun canBeAfter(token: TokenInterface): Boolean = false
}