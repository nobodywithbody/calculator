package calculator

class Variable : TokenInterface, Compounded {
    var value = ""

    override fun canBeFirst(): Boolean = true

    override fun canCompound(token: TokenInterface): Boolean = when (token) {
        is Variable, is Number -> true
        else -> false
    }

    override fun makeToken(char: Char): TokenInterface {
        val variable = Variable()
        variable.value = char.toString()

        return variable
    }

    override fun compound(token: TokenInterface) {
        if (token is Variable) value += token.value
        if (token is Number) value += token.value
    }

    override fun canBeAfter(token: TokenInterface): Boolean = when (token) {
        is Variable, is Number -> false
        else -> true
    }

    override fun canRepresent(char: Char): Boolean = char.isLetter()
}