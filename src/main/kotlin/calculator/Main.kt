package calculator

import java.math.BigInteger
import java.util.*
import kotlin.Exception

class Calculator() {
    var tokens = mutableListOf<TokenInterface>()
    val variables = mutableMapOf<String, String>()
    val postfixRepresentation = mutableListOf<TokenInterface>()

    fun isAssignment(expr: String): Boolean = expr.contains('=')

    fun isIdentifier(value: String): Boolean = value.matches(Regex("[a-zA-Z]+"))

    fun parseAssignment(expr: String) {
        val parts = expr.split("=")

        if (parts.size != 2) {
            throw Exception("Invalid assignment")
        }

        val name = parts.first().trim()
        val value = parts.last().trim()

        if (name.contains(Regex("[^a-zA-Z]"))) {
            throw Exception("Invalid identifier")
        }

        val valueIsDigits = value.matches(Regex("\\d+"))
        val valueIsIdentifier = isIdentifier(value)

        if (!(valueIsDigits || valueIsIdentifier)) {
            throw Exception("Invalid assignment")
        }

        if (valueIsIdentifier && variables[value] == null) {
            throw Exception("Unknown variable")
        }

        variables[name] = value
    }

    fun execute(expr: String): BigInteger {
        tokenizer(expr)
        convertToPostfix()
        return calculate()
    }

    fun convertToPostfix() {
        postfixRepresentation.clear()
        val stack = mutableListOf<TokenInterface>()

        fun isOperand(token: TokenInterface) = token is Number || token is Variable

        for (token in tokens) {
            if (isOperand(token)) {
                postfixRepresentation.add(token)
                continue
            }

            if (token is Parenthesis && token.isLeft) {
                stack.add(token)
                continue
            }

            if (stack.isEmpty()) {
                stack.add(token)
                continue
            }

            val last = stack.last()
            if (last is Parenthesis && last.isLeft) {
                stack.add(token)
                continue
            }

            if (token is Operator && last is Operator && token.isOperatorHigher(last)) {
                stack.add(token)
                continue
            }


            if (token is Operator && last is Operator && !token.isOperatorHigher(last)) {
                while (
                    (stack.last() is Operator && !token.isOperatorHigher(stack.last() as Operator)) ||
                    (stack.last() is Parenthesis && !(stack.last() as Parenthesis).isLeft)
                ) {
                    postfixRepresentation.add(stack.removeAt(stack.lastIndex))
                    if (stack.isEmpty()) break;
                }
                stack.add(token)
                continue
            }

            if (token is Parenthesis && !token.isLeft) {
                while (!(stack.last() is Parenthesis && (stack.last() as Parenthesis).isLeft)) {
                    postfixRepresentation.add(stack.removeAt(stack.lastIndex))
                    if (stack.isEmpty()) break
                }

                if (stack.isEmpty() || stack.last() is Parenthesis && !(stack.last() as Parenthesis).isLeft) {
                    throw Exception("Invalid expression")
                }

                stack.removeAt(stack.lastIndex)
                continue
            }
        }

        while (stack.isNotEmpty()) {
            val last = stack.last()
            if (last is Parenthesis) {
                throw Exception("Invalid expression")
            }
            postfixRepresentation.add(stack.removeAt(stack.lastIndex))
        }
    }

    fun calculate(): BigInteger {
        val stack = mutableListOf<BigInteger>()

        for (token in postfixRepresentation) {
            if (token is Operator) {
                val a = stack.removeAt(stack.lastIndex)
                val b = stack.removeAt(stack.lastIndex)
                stack.add(token.doOperation(b, a))

                continue
            }

            if (token is Variable) {
                stack.add(getVariableValue(token.value).toBigInteger())
                continue
            }

            if (token is Number) {
                stack.add(token.value.toBigInteger())
            }
        }

        return stack.last()
    }

    fun getVariableValue(name: String): String {
        var variableName = name.trim()

        while (true) {
            val value = variables[variableName] ?: throw Exception("Unknown variable")

            if (value.first().isLetter()) {
                variableName = value
                continue
            }

            return value
        }

    }

    fun tokenizer(expr: String) {
        tokens.clear()

        for (symbol in expr) {
            val type = TokenRegistry.getType(symbol) ?: throw Exception("Invalid expression")

            if (type is Whitespace) {
                continue
            }

            if (tokens.isEmpty() && type.canBeFirst()) {
                tokens.add(type)
                continue
            }

            if (tokens.isEmpty() && !type.canBeFirst()) {
                throw Exception("Invalid expression")
            }
            val last = tokens.last()

            if (last is Compounded && last.canCompound(type)) {
                last.compound(type)
                continue
            }

            if (!type.canBeAfter(last)) throw Exception("Invalid expression")

            tokens.add(type)
        }
    }
}

fun main() {
    val s = Scanner(System.`in`)
    val calc = Calculator()

    loop@ while (true) {
        val line = s.nextLine()
        if (line.isEmpty()) continue@loop

        val isCommand = line.first() == '/'

        val command = if (isCommand) line.substring(1) else ""

        if (isCommand) {
            when (command) {
                "exit" -> break@loop
                "help" -> println("Consider that the even number of minuses gives a plus, and the odd number of minuses gives a minus! Look at it this way: 2 -- 2 equals 2 - (-2) equals 2 + 2.")
                else -> println("Unknown command")
            }
            continue@loop

        }

        try {
            if (calc.isAssignment(line)) {
                calc.parseAssignment(line)
                continue
            }

            println(calc.execute(line))
        } catch (e: Exception) {
            println(e.message)
        }

    }

    println("Bye!")
}

