package calculator

class Parenthesis : TokenInterface {
    var isLeft = true

    override fun canCompound(token: TokenInterface): Boolean = false
    override fun canRepresent(char: Char): Boolean = (char == '(' || char == ')')
    override fun canBeFirst(): Boolean = isLeft
    override fun makeToken(char: Char): TokenInterface {
        val parenthesis = Parenthesis()
        parenthesis.isLeft = char == '('

        return parenthesis
    }

    override fun canBeAfter(token: TokenInterface): Boolean = when {
        token is Parenthesis && (!token.isLeft && isLeft) -> false
        else -> true
    }
}