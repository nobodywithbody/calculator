package calculator

import java.math.BigInteger

class Operator : TokenInterface, Compounded {
    val variants = arrayOf('*', '/', '+', '-')
    var value = '+'
    val isCompounded
        get() = value == '+' || value == '-'

    override fun canBeFirst(): Boolean = when {
        isCompounded -> true
        else -> false
    }

    override fun canCompound(token: TokenInterface): Boolean {
        if (token !is Operator) {
            return false
        }
        if ((token.isCompounded) && (isCompounded)) return true

        return false
    }

    override fun canRepresent(char: Char): Boolean {
        for (operator in variants) {
            if (operator == char) return true
        }

        return false;
    }

    override fun makeToken(char: Char): TokenInterface {
        val operator = Operator()
        operator.value = char

        return operator
    }

    override fun compound(token: TokenInterface) {
        if (token is Operator) {
            value = if (token.value == '-' && value == '-' || token.value == '+' && value == '+') '+' else '-'
        }
    }

    override fun canBeAfter(token: TokenInterface): Boolean = when (token) {
        is Operator -> false
        else -> true
    }

    fun isOperatorHigher(b: Operator): Boolean {
        if (value == b.value) return false

        for (operator in variants) {
            if (operator == value) return true
            if (operator == b.value) return false
        }

        throw Exception("Invalid expression")
    }

    fun doOperation(a: BigInteger, b: BigInteger): BigInteger = when (value) {
        '+' -> a + b
        '-' -> a - b
        '/' -> a / b
        '*' -> a * b
        else -> a
    }
}