package calculator

interface TokenInterface {
    fun canCompound(token: TokenInterface): Boolean
    fun canRepresent(char: Char): Boolean
    fun makeToken(char: Char): TokenInterface
    fun canBeFirst(): Boolean
    fun canBeAfter(token: TokenInterface): Boolean
}

interface Compounded {
    fun compound(token: TokenInterface)
}