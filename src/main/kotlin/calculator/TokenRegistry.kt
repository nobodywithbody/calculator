package calculator

object TokenRegistry {
    val typesOfToken = mutableListOf<TokenInterface>()

    init {
        register(Operator())
        register(Variable())
        register(Number())
        register(Parenthesis())
        register(Whitespace())
    }

    fun register(token: TokenInterface) {
        typesOfToken.add(token)
    }

    fun getType(char: Char): TokenInterface? {
        for (type in typesOfToken) {
            if (type.canRepresent(char)) {
                return type.makeToken(char)
            }
        }

        return null
    }
}